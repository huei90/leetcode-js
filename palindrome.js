/**
 * Url:
 * http://leetcode.com/2012/01/palindrome-number.html
 *
 * Title:
 * panlindrome
 *
 * Description:
 * Determine whether an integer is a palindrome. Do this without extra space.
 *
 * Contributor:
 * Huei Tan <huei90@gmail.com>
 *
 * Example:
 *
 * 1223221 => is palindrome
 * 1233321 => is palindrome
 * 1234444 => not palindrome
 */
(function () {
    var palindrome = function palindrome(integer) {
        return integer === parseInt(integer.toString().split('').reverse().join(''));
    };
})();
// palindrome(1223221) => true
// palindrome(1222111) => false
// this case in javascript is very simple!
