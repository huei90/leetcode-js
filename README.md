leetcode-js
===

Sometimes, Javascript can be simple but actually NOT!


The puzzles are come from

1. http://leetcode.com/
2. https://oj.leetcode.com/
3. others

Puzzle
===

1. [palindrome-number](https://github.com/huei90/leetcode-js/palindrome.js)
2. [Longest Palindromic Substring](https://github.com/huei90/leetcode-js/longest-palindrome-substring.js)
3. [Regular Expression Matching](https://github.com/huei90/leetcode-js/regular-expression-matching.js)
4. More...

Description Template
===

```js
/**
 * Url:
 * http://leetcode.com/2011/11/longest-palindromic-substring-part-i.html
 * http://leetcode.com/2011/11/longest-palindromic-substring-part-ii.html
 * http://en.wikipedia.org/wiki/Longest_palindromic_substring (wiki)
 *
 * Title:
 * Longest Palindromic Substring
 *
 * Description:
 * Given a string S, find the longest palindromic substring in S
 *
 * Contributor:
 * Huei Tan <huei90@gmail.com> - O(n^2)
 *
 * Best Solution:
 * O(n) - Manacher's algorithm (waiting to update)
 *
 * Example:
 *
 * bananas ＝> anana
 */
```
