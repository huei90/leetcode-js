/**
 * Url:
 * http://leetcode.com/2011/09/regular-expression-matching.html
 *
 * Title:
 * Regular Expression Matching
 *
 * Description:
 * Implement regular expression matching with support for ‘.’ and ‘*’.
 *
 * ‘.’ Matches any single character.
 * ‘*’ Matches zero or more of the preceding element.
 *
 * The matching should cover the entire input string (not partial).
 *
 * The function prototype should be:
 * bool isMatch(const char *s, const char *p)
 *
 * Contributor:
 * Huei Tan <huei90@gmail.com>
 *
 * Best Solution:
 * --
 *
 * Example:
 *
 * Some examples:
 * isMatch("aa","a") → false
 * isMatch("aa","aa") → true
 * isMatch("aaa","aa") → false
 * isMatch("aa", "a*") → true
 * isMatch("aa", ".*") → true
 * isMatch("ab", ".*") → true
 * isMatch("aab", "c*a*b") → true
 */
(function () {
    var isMatch = function isMatch(s, p) {

        if (s === p) {
            return true;
        } else {

        }
        // waiting to fix
        return false;

    };

    console.log(isMatch("aa", "a"));
    console.log(isMatch("aa", "aa"));
    console.log(isMatch("aaa", "aa"));
    console.log(isMatch("aa", "a*"));
    console.log(isMatch("aa", ".*"));
    console.log(isMatch("ab", ".*"));
    console.log(isMatch("aab", "c*a*b"));
})();
